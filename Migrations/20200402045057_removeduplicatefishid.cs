﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimalCrossing.Migrations
{
    public partial class removeduplicatefishid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FishHours_Fishs_FishId1",
                table: "FishHours");

            migrationBuilder.DropForeignKey(
                name: "FK_FishMonths_Fishs_FishId1",
                table: "FishMonths");

            migrationBuilder.DropIndex(
                name: "IX_FishMonths_FishId1",
                table: "FishMonths");

            migrationBuilder.DropIndex(
                name: "IX_FishHours_FishId1",
                table: "FishHours");

            migrationBuilder.DropColumn(
                name: "FishId1",
                table: "FishMonths");

            migrationBuilder.DropColumn(
                name: "FishId1",
                table: "FishHours");

            migrationBuilder.AlterColumn<int>(
                name: "FishId",
                table: "FishMonths",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "FishId",
                table: "FishHours",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.CreateIndex(
                name: "IX_FishMonths_FishId",
                table: "FishMonths",
                column: "FishId");

            migrationBuilder.CreateIndex(
                name: "IX_FishHours_FishId",
                table: "FishHours",
                column: "FishId");

            migrationBuilder.AddForeignKey(
                name: "FK_FishHours_Fishs_FishId",
                table: "FishHours",
                column: "FishId",
                principalTable: "Fishs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FishMonths_Fishs_FishId",
                table: "FishMonths",
                column: "FishId",
                principalTable: "Fishs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FishHours_Fishs_FishId",
                table: "FishHours");

            migrationBuilder.DropForeignKey(
                name: "FK_FishMonths_Fishs_FishId",
                table: "FishMonths");

            migrationBuilder.DropIndex(
                name: "IX_FishMonths_FishId",
                table: "FishMonths");

            migrationBuilder.DropIndex(
                name: "IX_FishHours_FishId",
                table: "FishHours");

            migrationBuilder.AlterColumn<long>(
                name: "FishId",
                table: "FishMonths",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FishId1",
                table: "FishMonths",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "FishId",
                table: "FishHours",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FishId1",
                table: "FishHours",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FishMonths_FishId1",
                table: "FishMonths",
                column: "FishId1");

            migrationBuilder.CreateIndex(
                name: "IX_FishHours_FishId1",
                table: "FishHours",
                column: "FishId1");

            migrationBuilder.AddForeignKey(
                name: "FK_FishHours_Fishs_FishId1",
                table: "FishHours",
                column: "FishId1",
                principalTable: "Fishs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FishMonths_Fishs_FishId1",
                table: "FishMonths",
                column: "FishId1",
                principalTable: "Fishs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
