﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimalCrossing.Migrations
{
    public partial class OptimizeFish : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VisibleDate",
                table: "Fishs");

            migrationBuilder.DropColumn(
                name: "VisibleTime",
                table: "Fishs");

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Fishs",
                type: "decimal(12,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Fishs",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Location",
                table: "Fishs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VisibleHourDesc",
                table: "Fishs",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VisibleMonthDesc",
                table: "Fishs",
                maxLength: 128,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FishHours",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FishId = table.Column<long>(nullable: false),
                    Hour = table.Column<int>(nullable: false),
                    FishId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FishHours", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FishHours_Fishs_FishId1",
                        column: x => x.FishId1,
                        principalTable: "Fishs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FishMonths",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FishId = table.Column<long>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    FishId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FishMonths", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FishMonths_Fishs_FishId1",
                        column: x => x.FishId1,
                        principalTable: "Fishs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Fishs_Name",
                table: "Fishs",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FishHours_FishId1",
                table: "FishHours",
                column: "FishId1");

            migrationBuilder.CreateIndex(
                name: "IX_FishMonths_FishId1",
                table: "FishMonths",
                column: "FishId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FishHours");

            migrationBuilder.DropTable(
                name: "FishMonths");

            migrationBuilder.DropIndex(
                name: "IX_Fishs_Name",
                table: "Fishs");

            migrationBuilder.DropColumn(
                name: "VisibleHourDesc",
                table: "Fishs");

            migrationBuilder.DropColumn(
                name: "VisibleMonthDesc",
                table: "Fishs");

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Fishs",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(12,2)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Fishs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "Location",
                table: "Fishs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "VisibleDate",
                table: "Fishs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VisibleTime",
                table: "Fishs",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);
        }
    }
}
