using AnimalCrossing.Db.Entities;
using GraphQL.Types;

namespace AnimalCrossing.GraphQL
{
    public class FishType:ObjectGraphType<Fish>
    {
        public FishType()
        {
            Field(x=>x.Id).Description("The Id of the Fish.");
            Field(x=>x.Name).Description("The Name of the Fish.");
            Field(x=>x.Price).Description("The Price of the Fish.");
            Field(x=>x.VisibleHourDesc).Description("The VisibleHourDesc of the Fish.");
            Field(x=>x.VisibleMonthDesc).Description("The VisibleMonthDesc of the Fish.");
            Field("visiblehours",x=>x.FishHours).Description("The VisibleHours of the Fish.");
            Field("visiblemonths",x=>x.FishMonths).Description("The VisibleHours of the Fish.");
            Field(x=>x.Location).Description("The Location of the Fish.");
            Field(x=>x.ShadowType).Description("The ShadowType of the Fish.");
        }
    }
}