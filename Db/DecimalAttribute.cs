using System;

namespace AnimalCrossing.Db
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DecimalAttribute:Attribute
    {
        /// <summary>
        /// 长度
        /// </summary>
        public int Length { get; set; } = 12;

        /// <summary>
        /// 小数部分位数
        /// </summary>
        public int Precision { get; set; } = 2;

        public DecimalAttribute(int length,int precision)
        {
            Length = length;
            Precision = precision;
        }

        public DecimalAttribute() { }
    }
}