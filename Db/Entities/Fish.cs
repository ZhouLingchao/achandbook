using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AnimalCrossing.Constants.Enums;

namespace AnimalCrossing.Db.Entities
{
    public class Fish
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, StringLength(64)]
        public string Name { get; set; }
        [Decimal(12, 2)]
        public decimal Price { get; set; }
        [StringLength(128)]
        public string VisibleMonthDesc { get; set; }
        public List<FishMonth> FishMonths { get; set; }
        [StringLength(128)]
        public string VisibleHourDesc { get; set; }
        public List<FishHour> FishHours { get; set; }
        public LocationType Location { get; set; }
        public ShadowType ShadowType { get; set; }
    }
}