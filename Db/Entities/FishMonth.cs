using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalCrossing.Db.Entities
{
    public class FishMonth
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        // 月份 1 = 一月
        public int Month { get; set; }
    }
}