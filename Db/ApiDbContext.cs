using System.Reflection;
using AnimalCrossing.Db.Entities;
using Microsoft.EntityFrameworkCore;

namespace AnimalCrossing.Db
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {
        }

        /// <summary>
        /// 配置数据库表约束等
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureDecimal(modelBuilder);
            modelBuilder.Entity<Fish>().HasIndex(x=>x.Name).IsUnique();
        }
        private void ConfigureDecimal(ModelBuilder modelBuilder)
        {
            var thisType = GetType();
            var props = thisType.GetProperties();
            foreach (var prop in props)
            {
                if (prop.DeclaringType != thisType) continue;
                var entityType = prop.PropertyType.GenericTypeArguments[0];
                var entityProps = entityType.GetProperties();
                foreach (var ep in entityProps)
                {
                    if (ep.PropertyType == typeof(decimal) || ep.PropertyType == typeof(decimal?))
                    {
                        var settings = (DecimalAttribute)ep.GetCustomAttribute(typeof(DecimalAttribute)) ?? new DecimalAttribute();
                        modelBuilder.Entity(entityType).Property(ep.Name).HasColumnType($"decimal({settings.Length},{settings.Precision})");
                    }
                }
            }
        }

        public DbSet<Fish> Fishs { get; set; }
        public DbSet<FishMonth> FishMonths { get; set; }
        public DbSet<FishHour> FishHours { get; set; }
    }
}